<?php

/**
 * Plugin Name: Mnumi for Wordpress
 * Plugin URI: https://bitbucket.org/mnumi/wordpress-mnumi-plugin
 * Description: The best web-to-print integration to get more customers.
 * Version: 2.0
 * Author: Mnumi
 * Author URI: http://mnumi.com
 * License: GPLv2
 */
if (!defined('ABSPATH')) {
    exit;
}

require_once (__DIR__ . '/classes/MnumiAPI.php');
require_once (__DIR__ . '/classes/CurlRequest.php');

new Mnumi();

class Mnumi {

    /**
     * Plugin version, used for cache-busting of style and script file references.
     *
     * @var string
     */
    const VERSION = '2.6';

    private $api;

    private $meta_key = 'mnumi_post_product';

    private $shortcode_mnumiwizard_defaults = array(
        'product' => '',
        'projects' => '',
        'countchange' => 1,
        'randomize' => 1,
        'count' => 1,
        'projecturl' => '',
        'counttype' => '',
        'countmin' => '',
        'countmax' => '',
    );

    public function __construct() {
        $this->init();

        $this->api = new MnumiAPI(
            get_option('MNUMI_API_HOST'),
            get_option('MNUMI_API_KEY')
        );
    }

    public function init() {
        add_action('admin_menu', array($this, 'mnumi_menu'));
        add_action('admin_init', array($this, 'mnumi_settings'));

        /* Fire our meta box setup function on the post editor screen. */
        add_action( 'load-post.php', array($this, 'post_meta_boxes') );
        add_action( 'load-post-new.php', array($this, 'post_meta_boxes') );

        /* change link for Mnumi products */
        add_filter( 'post_type_link', array($this, 'change_post_link'), 1000 );

        load_plugin_textdomain('mnumi', false, basename( dirname( __FILE__ ) ) . '/languages' );

        add_shortcode( 'mnumiWizard', array($this, 'shortcode_mnumiwizard') );
        add_shortcode( 'mnumiWizardLink', array($this, 'shortcode_mnumiwizard_link') );
    }

    public function change_post_link( $url, $post = false, $leavename = false ) {
        $post = get_post($post);

        /* Get the meta key. */
        $meta_key = $this->meta_key;

        if ( $post->post_type == 'post' || $post->post_type == 'project') {
            /* Get the meta value of the custom field key. */
            $meta_value = get_post_meta( $post->ID, $meta_key, true );

            if ($meta_value !== "") {
                return get_option('MNUMI_SHOP_HOST') . 'product/' . $meta_value;
            }
        }
        return $url;
    }

    public function post_meta_boxes() {
        add_action( 'add_meta_boxes', array($this, 'add_post_meta_boxes') );

        /* Save post meta on the 'save_post' hook. */
        add_action( 'save_post', array($this, 'save_post_product_meta'), 10, 2 );
    }

    public function add_post_meta_boxes() {
        foreach (array('post', 'project') as $type) {
            add_meta_box(
                'mnumi-post-class',      // Unique ID
                esc_html__( 'Mnumi product', 'mnumi' ),    // Title
                array($this, 'post_class_meta_box'),   // Callback function
                $type,         // Admin page (or post type)
                'side',         // Context
                'high'         // Priority
            );
        }
    }

    public function save_post_product_meta( $post_id, $post) {
        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST['mnumi_post_product_nonce'] ) || !wp_verify_nonce( $_POST['mnumi_post_product_nonce'], basename( __FILE__ ) ) )
            return $post_id;

        /* Get the post type object. */
        $post_type = get_post_type_object( $post->post_type );

        /* Check if the current user has permission to edit the post. */
        if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
            return $post_id;

        /* Get the posted data and sanitize it for use as an HTML class. */
        $new_meta_value = ( isset( $_POST['mnumi-post-product'] ) ? sanitize_html_class( $_POST['mnumi-post-product'] ) : '' );

        /* Get the meta key. */
        $meta_key = $this->meta_key;

        /* Get the meta value of the custom field key. */
        $meta_value = get_post_meta( $post_id, $meta_key, true );

        /* If a new meta value was added and there was no previous value, add it. */
        if ( $new_meta_value && '' == $meta_value )  {
            add_post_meta( $post_id, $meta_key, $new_meta_value, true );
        }
        /* If the new meta value does not match the old value, update it. */
        elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
            update_post_meta( $post_id, $meta_key, $new_meta_value );
            update_post_meta( $post_id, 'post_name', $new_meta_value );
        }
        /* If there is no new meta value but an old value exists, delete it. */
        elseif ( '' == $new_meta_value && $meta_value ) {
            delete_post_meta( $post_id, $meta_key, $meta_value );
        }
    }

    function post_class_meta_box($object, $box) {
        wp_nonce_field( basename( __FILE__ ), 'mnumi_post_product_nonce' );
        $value = get_post_meta( $object->ID, 'mnumi_post_product', true );

        include('mnumi_meta_box.php');
    }

    public function mnumi_settings() {
        register_setting('mnumi-settings-group', 'MNUMI_API_HOST', array($this, 'settings_group_validate_host'));
        register_setting('mnumi-settings-group', 'MNUMI_API_KEY', array($this, 'settings_group_validate_key'));
        register_setting('mnumi-settings-group', 'MNUMI_SHOP_HOST', array($this, 'settings_group_validate_host'));
        register_setting('mnumi-settings-group', 'MNUMI_WIZARD_HOST', array($this, 'settings_group_validate_host'));
        register_setting('mnumi-settings-group', 'MNUMI_WIZARD_KEY', array($this, 'settings_group_validate_key'));
        register_setting('mnumi-settings-group', 'MNUMI_WIZARD_ID');
    }

    function settings_group_validate_host($input) {
        if ($input == "") {
            add_settings_error(
                'hostError',
                esc_attr( 'settings_updated' ),
                __('Missing host', 'mnumi'),
                'error'
            );

            return false;

        }

        $isValid = preg_match('/^http[s]?\:\/\/[\w\d\.\-]+\/$/', $input);

        if ($isValid == false) {
            add_settings_error(
                'hostError',
                esc_attr( 'settings_updated' ),
                __('Incorrect host', 'mnumi') . ": " . $input,
                'error'
            );

            return $input;
        }

        return $input;
    }

    function settings_group_validate_key($input) {
        return $input;
    }

    function mnumi_menu() {
        add_options_page(
            __('Mnumi', 'mnumi'),
            __('Mnumi', 'mnumi'), 'manage_options', 'mnumi', array($this, 'mnumi_options'));
    }

    function mnumi_options() {
        include('mnumi_options.php');
    }

    /**
     * Add "mnumiWizard" shortcode
     * Example:
     *   [mnumiWizard product="slug" projects="abcdefgh" backUri="/wp/gallery" countChange=0 randomize=1]
     *
     * @param $attributes
     * @return string
     */
    function shortcode_mnumiwizard($attributes) {
        $defaults = $this->shortcode_mnumiwizard_defaults;
        $a = shortcode_atts($defaults, $attributes );

        $website = get_option('MNUMI_WIZARD_HOST');
        $shopUrl = get_option('MNUMI_SHOP_HOST');
        $secret = get_option('MNUMI_WIZARD_KEY');
        $id = get_option('MNUMI_WIZARD_ID');

        $params = array(
            'action' => 'clone',
            'backUrl' => $shopUrl . 'addWizard/%s/%s',
            'productName' => $a['product'],
            'wizards' => $a['projects'],
            'countChange' => $a['countchange'],
            'count' => $a['count'],
            'projectUrl' => $a['projecturl'],
            'countType' => $a['counttype'],
            'countMin' => $a['countmin'],
            'countMax' => $a['countmax'],
            'randomize' => $a['randomize'],
        );

        $parameter=base64_encode(json_encode($params));
        $signature=base64_encode(hash_hmac('sha1', $parameter, $secret, true));

        $result = $website .  sprintf('initOrder?parameter=%s&signature=%s', $parameter, $signature);

        if ($id) {
            $result .= '&id=' . $id;
        }

        return $result;
    }

    function shortcode_mnumiwizard_link($attributes) {
        $defaults = array_merge(
            $this->shortcode_mnumiwizard_defaults,
            array(
                'title' => 'MnumiWizard',
                'class' => '',
            )
        );

        $a = shortcode_atts($defaults, $attributes );

        $link = $this->shortcode_mnumiwizard($a);

        return sprintf('<a href="%s" class="%s">%s</a>', $link, $a['class'], $a['title']);
    }
}

