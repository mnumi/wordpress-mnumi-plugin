<?php
if (!defined('ABSPATH')) {
    exit;
}
?>
<p>
    <label for="mnumi-post-product"><?php __( "Choose Mnumi product", 'mnumi' ); ?></label>
    <br />
    <select name="mnumi-post-product" class="widefat">
        <option value=""></option>
        <?php foreach($this->api->getProductsList() as $category): ?>
            <optgroup label="<?php echo $category['name']; ?>">
                <?php foreach($category['products'] as $product): ?>
                    <option value="<?php echo esc_html($product['slug']); ?>"<?php echo ($product["slug"] == $value) ? ' selected' : ''?>>
                        <?php echo $product['name']; ?>
                    </option>
                <?php endforeach; ?>
            </optgroup>
        <?php endforeach; ?>
    </select>
</p>
<p id="mnumi-product-navi">
    <a href="#" id="mnumi-product-view" target="mnumiShop"><?php echo __('View product', 'mnumi'); ?></a>
    |
    <a href="#" id="mnumi-product-edit" target="mnumiPanel"><?php echo __('Edit product', 'mnumi'); ?></a>
</p>
<script type="application/javascript">
    jQuery(document).ready(function($){
        function checkMnumiProductSelection() {
            $('p#mnumi-product-navi').toggle($('select[name=mnumi-post-product]').val() !== "");
        }

        $('select[name=mnumi-post-product]').change(checkMnumiProductSelection);

        $('a#mnumi-product-view').click(function(e) {
            e.preventDefault();
            url = '<?php echo get_option('MNUMI_SHOP_HOST'); ?>product/'
                + $('select[name=mnumi-post-product]').val();
            window.open(url, 'mnumiShop');

        });

        $('a#mnumi-product-edit').click(function(e) {
            e.preventDefault();
            url = '<?php echo get_option('MNUMI_API_HOST'); ?>product/'
                + $('select[name=mnumi-post-product]').val();
            window.open(url, 'mnumiPanel');
        });

        checkMnumiProductSelection();
    });
</script>