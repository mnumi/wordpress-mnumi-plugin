=== Mnumi Integration ===

Contributors: jupeter
Tags: Mnumi, gallery, designer, shortcodes
Requires at least: 4.1
License: GPLv2 or later

Integration with Mnumi system for Print House Companies. Plugin allow to easy connect Wordpress storefront with Mnumi
e-commerce and on-line designer.

== Description ==

Mnumi Integration plugin allows to integrate Mnumi system with Mnumi e-commerce (MnumiShop) and on-line designer of
photo books and calendars (MnumiWizard).

== Installation ==

1. Upload the folder to the `/wp-content/plugins/` directory;
2. Activate the plugin through the 'Plugins' menu in WordPress;
3. In your WordPress go to "Settings" -> "Mnumi Integration" and enter your Mnumi API Code there.

== Screenshots ==

== Changelog ==

Mnumi Integration Version 2.5.0

== Frequently Asked Questions ==