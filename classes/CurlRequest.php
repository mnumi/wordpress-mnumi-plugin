<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * License: GPLv2
 */

/**
 * CurlRequest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CurlRequest
{
    const METHOD_GET = 'GET';

    /** @var string */
    private $url;

    /** @var string */
    private $method;
    private $username;
    private $password;
    private $requestParameters = array();
    private $response;
    private $header;

    public function __construct($method, $url)
    {
        $this->method = $method;
        $this->url = $url;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return $this
     */
    public function setAuthentication($username, $password)
    {
        $this->username = $username;
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function addRequestParameter($key, $value)
    {
        $this->requestParameters[$key] = $value;

        return $this;
    }

    /**
     * Execute
     *
     * @return $this
     */
    public function execute()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);

        curl_setopt($curl, CURLOPT_URL, $this->getUrl());

        if ($this->username != null) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "{$this->username}:{$this->password}");
        }

        $response = curl_exec($curl);

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $this->header = substr($response, 0, $headerSize);
        $this->response = substr($response, $headerSize);

        curl_close($curl);
        $curl = null;

        return $this;   
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return array
     */
    public function getJsonResponse()
    {
        return json_decode($this->getResponse(), true);
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return string
     */
    protected function getUrl()
    {
        $url = $this->url;

        if(count($this->requestParameters) > 0)  {
            if (!strpos($url, '?')) {
                $url .= '?';
            }

            $url .= http_build_query($this->requestParameters);
        }

        return $url;
    }
}