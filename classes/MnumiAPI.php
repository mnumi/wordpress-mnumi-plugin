<?php

/**
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * License: GPLv2
 */

/**
 * MnumiAPI class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class MnumiAPI {

    private $api_url;
    private $api_key;
    
    private $apiProducts = null;
    private $apiCategory = null;
    private $apiProductList = null;

    public function __construct($url, $key) {

        $this->api_key = $key;
        $this->api_url = $url . 'app.php/api/';
    }
    
    public function getKey()
    {
        return $this->api_key;
    }

    public function getProducts()
    {
        if($this->apiProducts == null)
        {
            $url = $this->api_url . 'products.json';

            $curl = new CurlRequest(CurlRequest::METHOD_GET, $url);
            $curl->setAuthentication(
                $this->api_key,
                $this->api_key
            );

            $this->apiProducts = $curl
                ->execute()
                ->getJsonResponse()
            ;
        }
        return $this->apiProducts;
    }
    
    public function getCategory()
    {
        if($this->apiCategory == null)
        {
            $url = $this->api_url . 'category.json';

            $curl = new CurlRequest(CurlRequest::METHOD_GET, $url);
            $curl->setAuthentication(
                $this->api_key,
                $this->api_key
            );

            $this->apiCategory = $curl
                ->execute()
                ->getJsonResponse()
            ;
        }
        return $this->apiCategory;
    }

    public function getProductsList() {
        if($this->apiProductList == null)
        {
            $categoryTmp = $this->getCategory();
            if (!$categoryTmp) {
                return false;
            }
            $category = array();
            foreach ($categoryTmp as $cat) {
                $category[$cat['id']] = $cat;
            }
            $products = $this->getProducts();
            if (!$products) {
                return false;
            }
            $return = array();
            foreach ($products as $product) {
                if (isset($category[(int) $product['category']]) && isset($product['slug'])) {
                    $return[(int) $product['category']]['name'] = $category[(int) $product['category']]['name'];
                    $return[(int) $product['category']]['products'][] = array(
                        'name' => (string) $product['name'],
                        'slug' => (string) $product['slug']
                    );
                }
            }
            $this->apiProductList = $return;
        }
        return $this->apiProductList;
    }
}
