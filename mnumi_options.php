<?php
if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="wrap">
    <h2><?php echo __('Mnumi for Wordpress Settings', 'mnumi'); ?></h2>
    <form method="post" action="options.php">
        <?php settings_fields('mnumi-settings-group'); ?>
        <?php do_settings_sections('mnumi-settings-group'); ?>

        <table class="form-table">
            <tr valign="top">
                <th scope="row"><?php echo __('MnumiShop Address (URL)', 'mnumi'); ?></th>
                <td>
                    <input class="regular-text" type="text" name="MNUMI_SHOP_HOST" value="<?php echo get_option('MNUMI_SHOP_HOST'); ?>" />
                    <p class="description"><?php echo __('Format', 'mnumi'); ?>: http://[your-shop-url]/</p>
                </td>
            </tr>
        </table>

        <h3 class="title"><?php echo __('MnumiPanel integration', 'mnumi'); ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><?php echo __('MnumiPanel Address (URL)', 'mnumi'); ?></th>
                <td>
                    <input class="regular-text" type="text" name="MNUMI_API_HOST" value="<?php echo get_option('MNUMI_API_HOST'); ?>" />
                    <p class="description"><?php echo __('Format', 'mnumi'); ?>: http://[your-panel-url]/</p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php echo __('MnumiPanel Secret key', 'mnumi'); ?></th>
                <td>
                    <input class="regular-text" type="text" name="MNUMI_API_KEY" value="<?php echo get_option('MNUMI_API_KEY'); ?>" />
                    <p class="description"><?php echo __('Secret key from MnumiPanel configuration', 'mnumi'); ?></p>
                </td>
            </tr>
        </table>

        <h3 class="title"><?php echo __('MnumiWizard integration', 'mnumi'); ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><?php echo __('MnumiWizard Address (URL)', 'mnumi'); ?></th>
                <td>
                    <input class="regular-text" type="text" name="MNUMI_WIZARD_HOST" value="<?php echo get_option('MNUMI_WIZARD_HOST'); ?>" />
                    <p class="description"><?php echo __('Format', 'mnumi'); ?>: http://[your-wizard-url]/</p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php echo __('MnumiWizard Secret key', 'mnumi'); ?></th>
                <td>
                    <input class="regular-text" type="text" name="MNUMI_WIZARD_KEY" value="<?php echo get_option('MNUMI_WIZARD_KEY'); ?>" />
                    <p class="description"><?php echo __('Secret key from MnumiWizard configuration file', 'mnumi'); ?></p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php echo __('MnumiWizard Secret id', 'mnumi'); ?></th>
                <td>
                    <input class="regular-text" type="text" name="MNUMI_WIZARD_ID" value="<?php echo get_option('MNUMI_WIZARD_ID'); ?>" />
                    <p class="description"><?php echo __('Optional: MnumiWizard identifier for secret key', 'mnumi'); ?></p>
                </td>
            </tr>
        </table>


        <?php submit_button(); ?>
    </form>

</div> 