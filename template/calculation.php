<?php if($simple === false): ?>
    <fieldset>
        <label><?= _e($resultArray['size']['label']);?>: </label>
        <?= _e($resultArray['size']['fieldLabel']);?> <?= _e($resultArray['sides']['fieldLabel']);?>
    </fieldset>
    <fieldset>
        <label><?= _e('Issue'); ?>: </label>
        <?=$resultArray['count']['value'];?> x <?=$resultArray['quantity']['value'];?> <?= _e('pc.'); ?>
    </fieldset>
    <?php $priceItems = $resultArray['priceItems']; ?>
    <?php foreach($priceItems as $key => $rec): ?>
        <?php if('MATERIAL' == $key): ?>
            <?php if(!$resultArray['fixedPrice']): ?>
            <fieldset>
                <label><?= _e($rec['label']); ?>:</label><br />
                <?=$rec['fieldLabel'];?>: <?=number_format($rec['price'], 2, ',', ' '); ?> zł
                <?php endif; ?>
            </fieldset>
        <?php elseif ('PRINT' == $key && isset($resultArray['square_metre'])): ?>
            <fieldset>
                <label><?= _e($rec['label']); ?>: </label><br />
                <?= $rec['fieldLabel']; ?> - <?= sprintf("%.2f", $resultArray['square_metre']); ?>m<sup>2</sup>: <?= $rec['price']; ?>
            </fieldset>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php if(isset($resultArray['tradersPrice']) && $resultArray['tradersPrice'] > 0): ?>
        <fieldset>
            <label><?= _e('Additional work'); ?>:</label>
            <?=number_format($resultArray['tradersPrice'], 2, ',', ' '); ?> zł
        </fieldset>
    <?php endif; ?>
<?php endif; ?>
<span class="amount">
<?php if(0 != $resultArray['summaryPriceNet']): ?>
    <?= number_format($resultArray['summaryPriceGross'], 2, ',', ' '); ?> zł
<?php else: ?>
    <?= _e('Error'); ?>
<?php endif; ?>
</span>



