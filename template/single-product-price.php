<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;
?>
<form id="buy_block" class="cart" method="post" enctype='multipart/form-data'>
<div class="product_attributes clearfix mnumi" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <?php foreach($groups as $id_attribute_group => $item):?>
            <?php if ($item['group_type'] != 'select'): ?>
                <?php if ($item['id'] == "QUANTITY"):?>
                    <?php if(@$item['static'] === true): ?>
                        <fieldset class="attribute_fieldset">
                            <label class="attribute_label"><?=$item['name'];?> :</label>
                            <div class="attribute_list">
                                <select name="qty" id="group_<?=$item['id'];?>" class="form-control attribute_mnumi no-print">
                                    <?php foreach ($item['attributes'] as $id_attribute => $group_attribute): ?>
                                        <option value="<?=intval($id_attribute);?>" <?= (( isset($get[$item['id']]) && $get[$item['id']] == $id_attribute) || $item['default'] == $id_attribute)?' selected="selected"':'';?> title="<?=$group_attribute;?>"><?=$group_attribute;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div> <!-- end attribute_list -->
                        </fieldset>
                    <?php else: ?>
                        <?php
                                if ( ! $product->is_sold_individually() )
                                        woocommerce_quantity_input( array(
                                                'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
                                                'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
                                        ) );
                        ?>
                    <?php endif; ?>
                <?php else: ?>
                    <p <?=($item['group_type'] == "hidden")?' style="display:none;"':'';?>>
                        <label><?=$item['name'];?> :</label>
                        <input type="<?=$item['group_type'];?>" name="<?=$item['id'];?>" id="<?=$item['id'];?>" class="text attribute_mnumi" value="<?=$item['default'];?>" style="border: 1px solid rgb(189, 194, 201);"/>
                        <span class="clearfix"></span>
                    </p>
                <?php endif; ?>
            <?php elseif ($item['group_type'] == 'select'): ?>
                <?php if(isset($item['attributes']) && $item['attributes']):?>
                    <?php if(count($item['attributes']) > 0): ?>
                        <fieldset class="attribute_fieldset">
                            <label class="attribute_label" for="group_<?=$item['id'];?>"><?=$item['name'];?> :</label>
                            <div class="attribute_list">
                                <select name="<?=$item['id'];?>" id="group_<?=$item['id'];?>" class="form-control attribute_mnumi no-print">
                                    <?php foreach ($item['attributes'] as $id_attribute => $group_attribute): ?>
                                        <option value="<?=intval($id_attribute);?>" <?= (( isset($get[$item['id']]) && $get[$item['id']] == $id_attribute) || $item['default'] == $id_attribute)?' selected="selected"':'';?> title="<?=$group_attribute;?>"><?=$group_attribute;?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php if ($item['id'] == "SIZE"):?>
                                    <div id="SIZE" style="display:none;">
                                        <p class="size_form">
                                            <label for="order_size_width">Szerokość</label>
                                            <input type="text" value="100" id="order_size_width" style="border: 1px solid rgb(189, 194, 201);" name="size_width" class="text attribute_mnumi" disabled> mm
                                        </p>
                                        <p class="size_form">
                                            <label for="order_size_height">Wysokość</label>
                                            <input type="text" value="100" id="order_size_height" style="border: 1px solid rgb(189, 194, 201);" name="size_height" class="text attribute_mnumi" disabled> mm
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div> <!-- end attribute_list -->
                        </fieldset>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        <p id="mnumiprice" class="price"></p>
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
        
</div>
    <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
    <button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button>
    <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
</form>