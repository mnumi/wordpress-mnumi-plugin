<script>
jQuery(document).ready(function() {
    jQuery('.product_attributes:not(.mnumi)').remove();
    if(jQuery('select[name="SIZE"] option:selected').text() == '- Customizable -')
    {
        jQuery('#SIZE').find('input').prop('disabled', false);
        jQuery('#SIZE').show();
    } else {
        jQuery('#SIZE').find('input').prop('disabled', true);
        jQuery('#SIZE').hide();
    }
    getProductCalculate();

    jQuery('select[name="SIZE"]').change(function () {
        if(jQuery('select[name="SIZE"] option:selected').text() == '- Customizable -')
        {
            jQuery('#SIZE').find('input').prop('disabled', false);
            jQuery('#SIZE').show();
        } else {
            jQuery('#SIZE').find('input').prop('disabled', true);
            jQuery('#SIZE').hide();
        }
    });
});
jQuery(document).on('change input', '.attribute_mnumi', function(e){
    e.preventDefault();
    requestMnumi = '';
    jQuery('.mnumi select, .mnumi input[type=hidden], .mnumi input[type=text], .mnumi #attributes input[type=radio]:checked').each(function(a, e) {
        requestMnumi += '/'+jQuery(e).attr('name') + '=' + jQuery(e).attr('value');
    });
    requestMnumi = requestMnumi.replace(requestMnumi.substring(0, 1), '?');
    url = window.location + '';

    // redirection
    if (url.indexOf('?') != -1)
        url = url.substring(0, url.indexOf('?'));

    // set ipa to the customization form
    window.location.hash = requestMnumi;
    getProductCalculate();

});

jQuery(document).on('click', '.product_quantity_up', function(e){
    e.preventDefault();
    setTimeout('getProductCalculate()',500);
});
jQuery(document).on('click', '.product_quantity_down', function(e){
    e.preventDefault();
    setTimeout('getProductCalculate()',500);
});
jQuery('.our_price_display').removeClass('our_price_display');
function getProductCalculate()
{
    jQuery('.single_add_to_cart_button').attr('disabled', 'disabled');
    var data = {
            'action': 'mnumi_calculation',
            'data': jQuery('#buy_block').serialize()
    };
    jQuery( "p#mnumiprice" ).html('<img alt="" src="data:image/gif;base64,R0lGODlhMgAyANU3APf39+/v7/Pz8+vr6+fn59/f3+Pj49LS0tvb287OztfX18rKyrq6usbGxsLCwqamppKSkr6+vra2tp6enq6urqqqqrKyso6OjqKiopaWlpqammlpaX19fXl5eYqKim1tbXFxcUlJSYaGhmFhYWVlZXV1dVlZWYKCglVVVV1dXU1NTT09PVFRUUVFRUFBQTk5OTExMSgoKDU1NS0tLRwcHPv7+////////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUI4NjExM0VDQTc1MTFFMUI5Q0Q4ODFDMzUxMUU3NDIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUI4NjExM0RDQTc1MTFFMUI5Q0Q4ODFDMzUxMUU3NDIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNS4xIFdpbmRvd3MiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmRpZDpDMzQ2MzYyNTYxQ0FFMTExOUUwQ0M3NDhBRTQ0OUIyQSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpDMzQ2MzYyNTYxQ0FFMTExOUUwQ0M3NDhBRTQ0OUIyQSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUKADcALAAAAAAyADIAAAb/QJtwSCwaj8ikcslsOp/QqHRKrVqhBATAVoOkDEKDonZt1h6gAtfztQEgHUEZOTgEhBiWY20CEzYfcjUAZHNCCxsWQgkkGTU1IihgCyYTZDUCcoY2AxwjCjYBICABkCwENhMhe5gCW1cBWosoIoQTHgM1DBgCNRMcYAABr27EUQcgE1tdLAucBcY1BgaXhELCmlIBGijKNgUPB2avwsNVAKoZmoVOAgQDxk6xmugdoFGY8EMA8UkHHxwSCJF2Rwq/gQEE5GoSwMKHERnUXBkUgIC1JwMebHjQD5/Cjk0KIGBHBYCrJwYoSIC2iUi5bEcOcADYgcKATTUIEChQoCCS/xoDFFDgoAHBJgDTXJFUggnklEFLW0q18WCChgkTFkSt4o4nTyUzSZDYQGErlVgHEqhVEmCAW30tH0GdSteGAAURIoCZA9St0yEEKlzgkGxvGaAKEiDoqaQABAgRFPg01FUBqiRNh2Ay+2QQkUdPDmBw8Jchz9JFpFnoIELgFQEFEixYGDLDBQk3uXA2U2cBtSYGGki0ocCC4c4Ixlx7txuJAhEZckOpgYCB8ioHPGQIZmDyz4RcEDg4gBoJgQsX9iaYAGruZzIADhzQVH14lAENLg+YgOGOAvKhoEIdeTUUwIBESElXRQ0RXGBUDRRQ0IsDD8iRgE12RdAATGUYgEwBA1sEUIEEj0iQwR0GWGCUDQY4cN0cBCxQkGji6DLBHQIw4MAlPDWHz0AJYACGjXcAEAGIdX02gCw1UFiQAQX4mOSUVFZp5ZVYEhEEACH5BAUKADcALAUABQAoACgAAAb/wJtwSCwSbMLJLSAMFIrQqHRoYt4gIeuNNO0SBTeAkNE6CLFMQYmIeHqhhdSQAFLeLtmbgkggQd5QYDcGYTcdYhcuTBYvZjccKwmAYQZINwhXQhUcQg5DEBtMCit/QgOEU4QVRCF7YlMCICYDQgksGl0AD0JITJheARoRQgcuKm5TNrt2k0QOISYEUgABlpaozUInI7RTJRfIAa/ZNwRaNVAADlwY0uSwWkUW78lvA+70ROhQDAvV+W9ePSkh4oYEQQCjnEOGL6EQAgO6QbHksKJFBhUyUnBk8ca+h0RAdBwiZsEQGyhTjixCceWbGgQkSaxICGEACUI23JjpENW9iHI3JjQg8HFkgAFuUOpzKYVABQYdGxIJwOBEKYsFFCAsEmGrRWxNFEg00CBeNrBeDFxgRs6AJ0BPMDBpSa4AXSgTNHRTYCFpF5P7CEktIuCAFV0UxCADdNdLgweoJEBlupMIAAvDbjTAMM5dAI7Z4hGggOrxqwUmTQ1+p8HK5lcNKA8x+YpnlyAAIfkEBQoANwAsBQAFACgAKAAABv/Am3BILBqJhKNyWdwsP8zoseGUWpUlRrQwqlyZl9Ig2hp6VIjveLjBCC0iYeIhnJSImu8tPnyMrh16QyRegoU3CzcpVgx/gkMLK3oMfI83IEQDSUQneZZLAA8hlXIcHRQBn0QAESkvJwZYh6oNJhuJqlcDB0cOuVICDQ8OAxcfFxe+v0QWGyQsLRRDDhl0y0MZJRgGAtfe38wS4uBCAgHnAR6mHaTeBgjwaeTzvwEKCvREDhgXJ7PX78wJefAPHAABBNbkkyKhwTIAURpAeCAvVw0DBAocqYABlzcABLpFSfBJoZBUSwY8kPaJlxUCDywMQamHgMsoFlgKUSYFHpFMm0sQQHwjQcgAjTeGCkE6QIFCmlYOMFjjwOWBojcQaBFyAKgeCx4dkLyRQOaNADxvFIj1CN8Qh3LM3lDg9ReCQ2WHdCWHdG6ETFeCAAAh+QQFCgA3ACwFAAUAKAAoAAAG/8CbcEgsDoaWT3HJbDY7wwdpOIE6r8RCUSGUDj2noQHr5DSEA0+lOxWWLMMOhszEeIYniBCTGpqGECh0TAQcazcRGUhdRIJER1cQDEMUSnQiG0QkD1gSHxFDDGN0C0MolmQgcINFByqZTgJFEJCsQndXBROHtmQUN3pFIhmgvVchF00OF8XGxgHOrAkI0balExcZGKXVSw4lfytFDxHQ3UQiJma157Ht1QkOERENWu9CBRoXEPwPF/8azty7kYDFixUrWgxcyLAhhQkJZAlZQAFDBgcS33lAyKJDOQYMDtywwZCAAw4pFDUcFCCByHYAMi5JQEHUuwEAzD1yIIHLQm8BNmTeGDXEpzEBOm8AwGLBQTQBBNhdkcAtWgACSZU5FULgQNYmBggMEVBAaBEARG80qCr1BgGxQgyYHVTg5Y2qBRLE1YvPXi8BDqgJOeATQVW7OdvS8RsAASQEfIf6jdluEj6BNxD4bZiXyFwmQQAAIfkEBQoANwAsBQAFACgAKAAABv/Am3BIJAqKoqJyyVw6kkPQ8HFpWpuHUyE6zHiuYIKGmGFwhSIzugJeUjLHG2Qy/AgdpOEk1WZ6JFMARRZVQhtsfTcPDkQnVgFDFxyJQg1Qd32AQgojfQseapRDfIkNiKJyk00EFBaoS5BFY0QIGhivTR8sTQu3uEQJGx2/xEMKxZQUYxQHyEUnIyYqtJrOQhcjl9bb3AsLDQncNwS+NxoUGBgTD+I3Ju3w2wQG3NVDBw4MFQvcGUQNNxwAjDekHMGDlT6cUoKAwcBiBkqsGMHPSIKH1hywCEGnSYAtrx4YpGCviABwuDLE8AcmQINmvybIgBAHS0Uh9NoEiHUjwww1g7BoHbshoCYToxJuhkFgjMASpkOcvhoqpADIIgVqAjCaSCpOJgPEEVAaTgiBsPEIUK2RKAgAIfkEBQoANwAsBQAFACgAKAAABv/Am3BILBIjF6NyyRwuMkNJUiiBNq/CAdF6s0xvmAkW+zgMKQ6hNztZDDOSsTFCGWLqXZFwwTEIKx1ySg9pNwkRc2JCJYI3AwxmQg1cAU0THkQdDFcJGJEAkVgRCkMcHlpXBw9ujZlcY5utQg8QlUwNDbJYEh8PRhKjukoLIhsaSwoRCcJFHF/M0DcEANFNCrEOEgwRBdVECx4lIylFDaTeQholr+jt6AUH8Qqo7RU3GscNEvsO5+4tIVqgcEfQna12vogYWLDAAQJ3E0LcSHEhgQAECroVFJIBxEZBAAwQqLYszhICBxAcjGbiRqghBhCMbHdgw0AiAKgRESAMzxFqaxplYbihCIsCP40AJJh5Q8WYAkFv8MQygEQgIRNY2BNkAOmNGkYqZOD54EUsWQEE6CQwdQBSDS8e3iDBgl6jAkwHzHS7J0SsBCqOyZoqJGcWpAM2XL2hLVoAf3yFdBixMhoAwje8NpQTBAAh+QQFCgA3ACwFAAUAKAAoAAAG/8CbcEgsEhcPo3LJHB6SwggxgmlalQjoTTqUaK9MBlEcJX4fXDCx4RhSuBGI8CAXSiAEtTFyMBYWQg1aF2RqgE6FVhUaRCJXBRZEBWoJekUWfZZDBSIXegqHmjcWE00CB5lCNqJKDhxGBA0NrEt9HRRLk7RGHhm7v0MGwGAOCjcLDg2pw0QnIB8SQwQHusw3DycTy9bc1gYFBnndQhgV5gkOEQ4J1cwFHywmJiPj9fbMEghFCggLwt0oUHzwJaSAuHu37ilc8qpJO2AnWCwZYCBAvRS4mAD4ldHOLCYHWa1QA6DiryRVrAQIaaPGFQEfzLTQNGBAFAgWCxgbVeoGhTWZwwCU2GDxgoohLojQYxXA4g0CJE4IuRDCab0GKMhAqLqkzS8NK3RRtTrOQYUhGVQ43dYkCAAh+QQFCgA3ACwFAAUAKAAoAAAG/8CbcEgsFi3GpHJJlAwTSOEiwqwqGQFhojJ0UK1MwcIwPBy0UQFDMWQ0wEbFWchYCBuPIaa7hxcBCUQISQxCAxMOfjcIBUMIdkIATBIURBOQTA1khn6NNwQTeVYGDZ6KQgQaomAGCpKnNw6VRAJFBAavsEsJGoVFCgUDukoVvUoECJvDQw+ry8/QYI9CCHI3wtFDCg8aHh5fNwHB2UMUEBaD5OrrWUO1605CFgVmB+PrHB8fJCDr/v/q3hQZYGAAgnbkKIAowWHIgAEI/Z35BtAPgHfQIIAzAiBAxGceQJxIIgCjOg4l4ik54EGZIgYqrSxwgYLAsBBwHKwwsemAzVJ/BVSw2MQAxcYlHYqg8KWkQAVsAVSQqNWgz08jJog4mANmQghMQzKgEIJBxRAQG3QFbRjug4ckY4dsmKWowARsN1IoASFiXZ8kJz6sSxAzK6wgACH5BAUKADcALAUABQAoACgAAAb/wJtwSCwSEQ6jcskcGhZDBWOISDSvyoFVqEgKDw0sFjEYEgpcInSYOIiNBfQXwY0MGQJhQhJ4GwkGQgZlNwNyCGs3DG5+A4Q3gE01Qw1hQgMSjEwDCo95YgpEFFNYAXR+RhYSjY+oNwleTAJ9rmIPlkMCg5+1cBa3SjYBrb1CBBISBFe8xUK0RQEAzag1AwYGBMzTQxIUExOJAATS20QRD6Tl6utKAe7a6g0R83Zxg+xDGhwnHiL4/+sQWMAQaF2CBsqMXUgRAsYHOeoYnOCQwQKCAyNIWAQohIEGjn4QdLigDtwSAh5eoEg3bQIHkkUwutDwrFwBCP6OVIB4A0IxZwexbizYssTEhmIRNlB4EwIEMSwFaDEosRQLiUcjiDYRMWFIBBBB/xQx8eFKhY4fErmqgGLIiRJCug5JMYRDh2Ywb5TIIEQDiyEjLCng0IvAgyEHjg4x4dYnO7ZE/grxcAKkEBK9ggAAIfkEBQoANwAsBQAFACgAKAAABv/Am3BILBYTxqRyqSwghwimVKkYFoiIw3RqWD5v0a10MCB2hY2hg5gWu4naN7Ms/t4i2yuxlicy1lsEdHJCf4SHCnaHiwgSSQEAi0oODIpEApJFDW2ZmRgSg51UYCguKSYaAaJEDhIVD1oABxAmIGGrNxINZ0WRuL+LEMLCDMCYQw0qLisrLRDANxgeFzca0Ne4FdhDqts3Fx4Wxd5WNw/kYhmiBedL6r8PGRh6RSrXExqA6FskmSDjUhKM2CAKj5QSRQhu03djRAchCSgYiWBByJo4NyxJSUGEg5AHCiUO8eBhEcchJ4iMGPKh3CKRaoRMaHlDwLMhGgmFGDLh5I0Jd7+qDKmAkFAQACH5BAUKADcALAUABQAoACgAAAb/wJtwSCwSBwijcskcBpJCApFgaFqX1ehhOChcrwIiIKAdGqTCQfZLFISFhkE0URYWFOxbAC0EyPU1QgJ/Bl5CB2tXChsQbWwFhnoLkUsXNBlDNm9WA2SHdFcQMR55RQEJoEoCiRkplKV4TAsoG1tRpUwGDrGCFS0uEIm4RwmTSgEQKiLDRgMHB55LCrbM1dZKfzcjIUIP10YECwsODEUj30YHC9ToTTbt1RhEDfBDCgnPByZEGvVCEhgCVvBHEF4VCwQPZLuhYYOQDv4aVJgQoYEBBCBA3BBWL4EEdgW/TGgngZcSFiPofRPAAEMFPnYcFqRAIdVGCUQQeGO2wIhJYiMorDW4oNIKHo1CoJQaygbpDQcc2Lgc0sBDzyYMopVYdkNBOT1rvi5oZM0phAtCKEC8IaHEEA2YmIEYKYTsjQcf4JyglqGoNRFXdw4ZaO+bhbzdEN/AELfe1RsWotoBySQIACH5BAUKADcALAUABQAoACgAAAb/wJtwSCQSCkQDsVZsOp/CDGowVA4F0GzxMLSEEtUhgKrVKlQYoWEDCQsFATG5XASxCMLNB3tDCANWQnh0TQohHkIaJINDA4x+hEMaQyIrXH1ajH2aThoyQwQaYHQAQwVIZQ8wbZFFCJCEJK1GgU4HIx2ws1ABCqhDEiy7WqeckjcXw00Dv8rOQhXPhANUGy8kG9JOrwcLgRgbHNpECL7j5+jpWqVEIyoqISoP6QENEvcM6vrjEfnoCrpuPAAxIsSJdAokUDhw6YSIfYIUNIAYCUI0bQuaFcFAosMoaQ0YRHByggSFdAMYLGjiAN0lIaeyfEg27MA8QhsyxGk1J8GEWY9P8IjYuQARHQr+biTQoEDLREoZhjRgQuTA0wOThiCopYWBBysTsjKg6YDmjQoXd3VIe1YIBaM3IEAiwLZV0gIXLkkwS8FCuggluogTYuHmuWYMWAkJCCUIADs=" />');
    jQuery.ajax({
        url: woocommerce_params.ajax_url,
        data: data,
        type: "POST"
    }).done(function(data) {
        jQuery( "p#mnumiprice" ).html(data);
        jQuery('.single_add_to_cart_button').attr('disabled', false);
    });
}
</script>